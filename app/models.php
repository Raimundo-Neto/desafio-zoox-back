<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 05:40
 */

declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        \App\Domain\Models\Cidade\Cidade::class => \DI\autowire(\App\Domain\Models\Cidade\Cidade::class),
        \App\Domain\Models\Estado\Estado::class => \DI\autowire(\App\Domain\Models\Estado\Estado::class),
    ]);
};
