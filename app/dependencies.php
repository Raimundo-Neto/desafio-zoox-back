<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use  Jenssegers\Mongodb\Connection;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Factory as ValidatorFactory;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        ArrayLoader::class => DI\autowire(ArrayLoader::class),
        Translator::class => DI\autowire(Translator::class),
        ValidatorFactory::class => DI\autowire(ValidatorFactory::class),
        ValidationException::class=> DI\autowire(ValidationException::class),
        \App\Application\Validations\Cidade\CidadeValidation::class => DI\autowire( \App\Application\Validations\Cidade\CidadeValidation::class ),

    ]);

};
