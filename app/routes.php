<?php
declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Domain\Models\Bootstrap;
use App\Domain\Models\User\User;
use App\Domain\Models;

return function (App $app) {


    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) use($app){
        $response->getBody()->write('success');
        return $response;
    });
    $app->addBodyParsingMiddleware();

    $app->group('/cidades', function (Group $group) {
    $group->get('/page/{page}',\App\Application\Actions\Cidade\CidadeListAction::class);
        $group->post('/filtrar/page/{page}',\App\Application\Actions\Cidade\CidadeFiltragemAction::class);
        $group->get('/show/{id}',\App\Application\Actions\Cidade\CidadeShowAction::class);
        $group->delete('/delete/{id}',\App\Application\Actions\Cidade\CidadeDeleteAction::class);
        $group->post('/create',\App\Application\Actions\Cidade\CidadeCreateAction::class);
        $group->put('/update',\App\Application\Actions\Cidade\CidadeUpdateAction::class);

    });

    $app->addBodyParsingMiddleware();
    $app->group('/estados', function (Group $group) {
        $group->get('/page/{page}',\App\Application\Actions\Estado\EstadoListAction::class);
        $group->post('/filtrar/page/{page}',\App\Application\Actions\Estado\EstadoFiltragemAction::class);
        $group->get('/show/{id}',\App\Application\Actions\Estado\EstadoShowAction::class);
        $group->delete('/delete/{id}',\App\Application\Actions\Estado\EstadoDeleteAction::class);
        $group->post('/create',\App\Application\Actions\Estado\EstadoCreateAction::class);
        $group->put('/update',\App\Application\Actions\Estado\EstadoUpdateAction::class);

    });
    $app->addBodyParsingMiddleware();
};
