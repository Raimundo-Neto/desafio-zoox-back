<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'db' => [
                'mongo'=>[
                    'driver' => 'mongodb',
                    'host' => '127.0.0.1',
                    'port' => 27017,
                    'database' =>  'desafioZoox',
                    'username' => 'admin',
                    'password' => '123456',
                    'options' => [
                        'database' => 'admin',
                    ]

],


            ],
        ],
    ]);
};
