<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 11/10/2020
 * Time: 20:02
 */

namespace App\Application\Actions\Cidade;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class CidadeListAction extends  CidadeAction
{

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {

        return $this->responseJson($this->repository->index( (int) $this->args['page']));
    }
}