<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 22:21
 */

namespace App\Application\Actions\Cidade;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class CidadeFiltragemAction extends CidadeAction
{

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        if($this->dataRequest) {
            return $this->respondWithData($this->repository->filtrar($this->dataRequest['nome'], $this->dataRequest['uf'], $this->args['page']));
        }
        return $this->respondWithData($this->repository->index());
    }
}