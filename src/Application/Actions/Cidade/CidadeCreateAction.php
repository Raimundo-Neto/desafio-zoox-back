<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 08:33
 */

namespace App\Application\Actions\Cidade;


use App\Domain\DomainException\DomainRecordNotFoundException;
use App\Domain\Repository\Estado\EstadoRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class CidadeCreateAction extends  CidadeAction
{

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $this->validator->validate($this->dataRequest);

        if($this->verificarExistenciaEstado($this->dataRequest['uf'])){

            return $this->responseJson($this->repository->create( $this->dataRequest));

        }
    }


    protected  function  verificarExistenciaEstado($id){

        $estado = $this->estadoRepository->show($id);
        if(is_array($estado)){

            return true;

        }

        return new \Exception('Estado selecionado inválido');



    }


}