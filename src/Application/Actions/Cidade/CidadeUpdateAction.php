<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 09:09
 */

namespace App\Application\Actions\Cidade;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class CidadeUpdateAction extends  CidadeAction
{

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $this->validator->validate($this->dataRequest);

        if($this->verificaExistenciaEstado($this->dataRequest['estado'])){

            return $this->responseJson($this->repository->update($this->dataRequest));

        }
    }
}