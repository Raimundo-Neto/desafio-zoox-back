<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 11/10/2020
 * Time: 19:24
 */

namespace App\Application\Actions\Cidade;


use App\Application\Actions\Action;
use App\Application\Validations\Cidade\CidadeValidation;

use App\Domain\Repository\Cidade\CidadeRepository;
use App\Domain\Repository\Estado\EstadoRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

abstract class CidadeAction extends  Action
{


protected  $estadoRepository;

    public function __construct(CidadeRepository $cidadeRepository, EstadoRepository $estadoRepository, CidadeValidation $cidadeValidate)
    {

        $this->repository =  $cidadeRepository;
        $this->estadoRepository = $estadoRepository;
        $this->validator = $cidadeValidate;

    }




}