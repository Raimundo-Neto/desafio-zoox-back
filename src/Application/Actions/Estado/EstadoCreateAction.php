<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 09:21
 */

namespace App\Application\Actions\Estado;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class EstadoCreateAction extends  EstadoAction
{

    protected function action(): Response
    {
        $this->validator->validate($this->dataRequest);


        if($this->verificaExistenciaEstado($this->dataRequest['nome'],$this->dataRequest['uf']) == false){

            return $this->responseJson($this->repository->create($this->dataRequest));

        }

        return $this->responseJson('Estado já existe');
    }
}