<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 22:51
 */

namespace App\Application\Actions\Estado;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class EstadoSelectAction extends  EstadoAction
{

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        // TODO: Implement action() method.
    }
}