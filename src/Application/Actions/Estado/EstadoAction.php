<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 09:20
 */

namespace App\Application\Actions\Estado;


use App\Application\Actions\Action;


use App\Application\Validations\Estado\EstadoValidation;
use App\Domain\Repository\BaseRespository;
use App\Domain\Repository\Cidade\CidadeRepository;
use App\Domain\Repository\Estado\EstadoRepository;


abstract class EstadoAction extends  Action
{

    public function __construct( EstadoRepository $estadoRepository,CidadeRepository $cidadeRepository, EstadoValidation $estadoValidation)
    {
        $this->repository =  $estadoRepository;
        $this->cidadeRepository = $cidadeRepository;
        $this->validator = $estadoValidation;
    }

    protected  function  verificaExistenciaEstado($nome,$uf){

        if(is_array($this->repository->buscar($nome,$uf))){

            return true;
        }
        return false;
    }


}