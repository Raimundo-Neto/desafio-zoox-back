<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 09:21
 */

namespace App\Application\Actions\Estado;


use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class EstadoDeleteAction extends  EstadoAction
{

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        return $this->respondWithData($this->repository->delete($this->args['id']));
    }
}