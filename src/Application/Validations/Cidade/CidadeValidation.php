<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 10:00
 */

namespace App\Application\Validations\Cidade;


use App\Application\Validations\Validation;


class CidadeValidation extends Validation
{

    protected $rules = [
        'nome' => ['required', 'String'],
        'uf' => ['required']
    ];

    protected $messages = array(
        'nome.required' => 'Nome é obrigatório',
        'validation.required' => 'Preencha todos os campos obrigtórios',
        'uf.required' => 'O estaduo é campo obrigatório',
        'uf.string' => 'Campo estado é invalido',
    );


}