<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 10:01
 */

namespace App\Application\Validations;

use http\Exception\BadQueryStringException;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Factory as ValidatorFactory;


abstract class Validation
{
    protected $rules;
    protected $messages;



    public function validate(array $source)
    {
        $translator = new Translator(new ArrayLoader(), 'pt_br');
        $validatorFactory = new ValidatorFactory($translator);

        $validator = $validatorFactory->make($source, $this->rules,$this->messages);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        }
    }


}