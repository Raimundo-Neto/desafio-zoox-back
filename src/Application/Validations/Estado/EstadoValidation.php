<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 13/10/2020
 * Time: 16:23
 */

namespace App\Application\Validations\Estado;


use App\Application\Validations\Validation;

class EstadoValidation extends  Validation
{

    protected $rules = [
        'nome' => ['required', 'String'],
        'uf' => ['required']
    ];

    protected $messages = array(
        'nome.required' => 'Nome é obrigatório',
        'validation.required' => 'Preencha todos os campos obrigtórios',
        'uf.required' => 'O estaduo é campo obrigatório',
        'uf.string' => 'Campo estado é invalido',
    );


}