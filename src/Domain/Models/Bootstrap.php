<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 11/10/2020
 * Time: 14:21
 */
namespace  App\Domain\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Jenssegers\Mongodb\Connection;


final class  Bootstrap {


    public  static  function  load($container){

        $settings = $container->get('settings');
        $capsule = new Capsule();
        $config = $settings['db']['mongo'];


        $capsule->getDatabaseManager()->extend('default', function(  $config ) {

           return new Connection( $config );
       });

        $capsule->addConnection($config );


        $capsule->setEventDispatcher(new Dispatcher(new Container()));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();


    }






}