<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 11/10/2020
 * Time: 21:47
 */

namespace App\Domain\Models\Cidade;


use Jenssegers\Mongodb\Eloquent\Model;

class Cidade extends  Model
{

protected  $fillable = ['nome','uf'];
protected $primaryKey = '_id';
protected  $table = 'cidades';
public $timestamps = true;

}