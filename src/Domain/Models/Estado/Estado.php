<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 07:19
 */

namespace App\Domain\Models\Estado;


use Jenssegers\Mongodb\Eloquent\Model;

class Estado extends  Model
{

    protected $primaryKey = '_id';
    protected  $fillable = ['nome','uf'];

    public $timestamps = true;
    protected  $table = 'estados';

}