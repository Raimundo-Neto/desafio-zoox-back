<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 05:57
 */
declare(strict_types=1);

namespace App\Domain\Repository\Cidade;

use App\Domain\Models\Cidade\Cidade;
use App\Domain\Repository\BaseRespository;
use App\Domain\Repository\DefaultRepository;


class CidadeRepository extends DefaultRepository
{

    public function __construct(Cidade $cidadeModel)
    {
        $this->model = $cidadeModel;
    }

    public function filtrar($nome = null, $uf = null, $page = 0)
    {

        if ($nome) {

            $this->model->where('nome', $nome);
        }

        if ($uf) {

            $this->model->where('estado_id', $uf);

        }


        return $this->adicionaPaginacao($this->model->skip($page * $this->paginate)->take($this->paginate)->get());


    }






}