<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 12:10
 */

namespace App\Domain\Repository;


/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 05:58
 */



use Jenssegers\Mongodb\Eloquent\Model;

abstract class DefaultRepository
{
    protected $model;
    protected $paginate = 10;

    public function __construct(Model $model)
    {
        $this->model = $model;

    }

    public function index($page=0)
    {

        return $this->adicionaPaginacao($this->model->skip($page * $this->paginate)->take($this->paginate)->get(),$page);

    }

    public function show($id)
    {

        return $this->model->find($id);

    }

    public function delete($id)
    {

        return $this->model->destroy($id);

    }


    public  function  adicionaPaginacao($dados,$pagina=0){

        $paginacao['current_page'] = $pagina;
        $paginacao['to'] = $pagina +1;
        $paginacao['last_page'] = $pagina -1;
         $paginacao['data'] =$dados;
         return $paginacao;


    }

    public  function  create($data){


        $this->model->uf = $data['uf'];
        $this->model->nome = $data['nome'];
        return $this->model->save();
    }

    public  function  update($data){

        $update = $this->model->find($data['id']);
        $update->nome = $data['nome'];
        $update->uf = $data['uf'];
        return $update->save();
    }


}