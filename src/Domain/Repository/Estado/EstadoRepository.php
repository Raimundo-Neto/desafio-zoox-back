<?php
/**
 * Created by PhpStorm.
 * User: rai
 * Date: 12/10/2020
 * Time: 05:57
 */

declare(strict_types=1);

namespace App\Domain\Repository\Estado;


use App\Domain\Models\Estado\Estado;
use App\Domain\Repository\DefaultRepository;


class EstadoRepository extends  DefaultRepository
{

    public function __construct(Estado $estadoModel)
    {
        $this->model = $estadoModel;
    }

    public function filtrar($nome = null, $uf = null, $page = 0)
    {

        if ($nome) {

            $this->model->where('nome', $nome);
        }

        if ($uf) {

            $this->model->where('_id', $uf);

        }


        return $this->adicionaPaginacao($this->model->skip($page * $this->paginate)->take($this->paginate)->get());


    }

    public  function  buscar($nome,$uf){

        return $this->model->where('nome',$nome)->where('uf',$uf)->get();


    }



}